# 数式をMATLABで実装する

## 目的

基本的には，データ解析などで，新しい手法を使う場合は，MATLAB付属の関数，toolbox，公開されているものを使うのが望ましい．
なぜなら，そっちの方が間違いがない（場合が多い）し，高速に実装されていることが多い．

しかし，論文などで数式が出てきたら，怖がらずに実装する方法を身につけることで，いろいろな解析を試すことができる．
その力を本実験では身につけてもらう．
特に，MATLABの特徴である「行列計算が高速である」ことを意識して，コードを書く訓練をする．

## 数式を読み解くヒント

数式を書くルールは分野ごとに異なるが，知っておくと数式の理解が容易くなる．
例えば，信号処理の分野では，

- 小文字+斜体 $x, y, z$: 変数
- 大文字+斜体 $X, Y, Z$: 定数，確率変数
- 小文字+太字 $\mathbf{x}, \mathbf{y}, \mathbf{z}$: ベクトル
- 大文字+太字 $\mathbf{X}, \mathbf{Y}, \mathbf{Z}$: 行列
- 大文字+カリグラフィ $\mathcal{X}, \mathcal{Y}, \mathcal{Z}$: 集合

を表す場合が多い．
基本的には，どのようなルールを採用しても構わないが，

- その分野の一般的なルールを採用すること，
- 文章中でルールを変更しないこと，

は意識したい．

その他に，今回の文章で出てくるのは，

- $\mathbf{x}^{\top}, \mathbf{X}^{\top}$: ベクトルや行列の転置
- $[\mathbf{A}]_{n,m}$: 行列 $\mathbf{A}$ の $n$ 行・ $m$ 列に位置する要素
- $f(t), x(t)$: 変数 $t$ を持つ連続関数，連続信号
- $f[t], x[t]$: 変数 $t$ を持つ離散関数，離散信号
- $\mathbb{Z}$: 整数の集合
- $\mathbb{Z}^{+}$: 正の整数の集合
- $\mathbb{Z}^{-}$: 負の整数の集合
- $\mathbb{R}$: 実数の集合（簡単に言うとすべての少数）
- $\mathbb{C}$: 虚数の集合（$a + j b$ で表されるすべての数）
- $\mathbb{R}^{N \times M}$: サイズ $M \times N$ （行数 $N$ ，列数 $M$ ）で，各要素が実数である行列

である．
あまり，意味は分からなくても良い．

## ターゲットとなる数式

今回は，信号処理の基本中の基本，離散フーリエ変換（Discrete Fourier Transform; DFT）をMATLABの関数として，実装してもらう．
DFTは，離散信号を複素関数に移す直交写像であり，
\\[
	y[k] = \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j\frac{ 2 \pi k n}{N} },
	\quad k = 0, \ldots, N-1
\\]
と定義される．
ここで，

- $x[n]$: 時間インデックス $n$ で観測された信号，
- $y[k]$: 周波数インデックス $k$ における離散フーリエ変換後の信号，
- $j$: 虚数単位，
- $N$: 信号長， $N \in \mathbb{Z}^{+}$

$\frac{2 \pi}{N}$ は定数なので， $\omega = \frac{2 \pi }{N}$ とすると，
\\[
	y[k] = \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j \omega kn },
	\quad k = 0, \ldots, N-1
\\]
フーリエ変換は，時間領域の信号を周波数領域に変換する写像と呼ばれており，周波数解析において最もスタンダードな変換である．

MATLABの関数では，高速フーリエ変換（Fast Fourier Transform; FFT）が，`fft `として実装されている．
DFTとFFTの違いはWikipediaでも参照されたい．
とにかくFFTは速い．

## 行列演算としてDFTを実装する

### サイズを確認する

まず，行列演算として一番大事なことは，行列のサイズを意識することである．
サイズが分かれば，どのような演算をすればよいかイメージできる．

まず，入力信号 $x[n]$ である．
フーリエ変換の定義式を見ると， $n$ は $0$ から $N-1$ までの値を持っている（ $\sum$ の範囲に注目）ので， $x[0], x[1], x[2], \ldots, x[N-2], x[N-1]$ の値を使うことが分かる．
これらは $N$ 個の実数なので，サイズ $N \times 1$ のベクトルに格納できる．
（注: ベクトルは基本的に縦ベクトルで考える．）
このベクトルを $\mathbf{x} \in \mathbb{R}^{N \times 1}$ と書くことにする．

![](figs/1.pdf)

出力信号 $y[k]$ は， $x[n]$ と同様に， $y[0], y[1], y[2], \ldots, y[N-2], y[N-1]$ と $N$ 個の値が出力される（ 定義式の $k = 0, \ldots, N-1$ という箇所に注目）
したがって， サイズ $N \times 1$ のベクトルに格納できる．
このベクトルを $\mathbf{y} \in \mathbb{R}^{N \times 1}$ と書くことにする．

基本的に， $\sum$ が出てくる計算は，
\\[
	\mathbf{y} = \mathbf{A} \mathbf{x}
\\]
と表せる．

![](figs/2.pdf)

ちなみに， $\sum$ が2回出てくると
\\[
	\mathbf{y} = \mathbf{x}^{\top} \mathbf{A} \mathbf{x}
\\]
と表せる場合が多い．

![](figs/quad.pdf)

今回は， $\mathbf{y} = \mathbf{A} \mathbf{x}$ で表せるとたかを括って，考えてみる．

実質， $\mathbf{y} = \mathbf{A} \mathbf{x}$ を満たせる $\mathbf{A}$ は，サイズ $1 \times 1$ か， $N \times N$ だけである．

![](figs/1x1.pdf)

![](figs/nxn.pdf)

さすがにここで， $1 \times 1$ は無いので， $\mathbf{A} \in \mathbb{C}^{N \times N}$ と推定できる．
まだ，この段階では， $\mathbf{A}$ は実数か虚数か不明だが，実数は虚数を含んでいる（ $\mathbb{R} \in \mathbb{C}$ ）ため， $\mathbf{A}$ は虚数とした．

### 行列の中身を考える

定義式を読み解いて行列 $\textbf{A}$ の要素を求める．
理想的には，すべての要素が一つの数式（行・列番号を変数とする）で表せれば良い

$$
[\textbf{A}]_{n,m} = f(n, m)
$$

出力値 $y[0]$ がどのように計算されるか考える．
$y[0]$ はどのベクトルとの演算で求まるかというと，

![](figs/a0.pdf)

の黒い部分の内積である．
定義から $y[0]$ の演算をたどると，
\\[
\begin{split}
	y[0] &= \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j\omega n 0 }
	\\
	&= \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{0}
	\\
	&= \frac{1}{\sqrt{N}}
		\left[
		\begin{matrix}
			e^{0} & e^{0} & e^{0} & \cdots & e^{0}
		\end{matrix}
		\right]
		\left[
		\begin{matrix}
			x[0] \\ x[1] \\ x[2] \\ \vdots \\ x[N-1]
		\end{matrix}
		\right]
	\\
	&= \mathbf{a}_{0}^{\top} \mathbf{x}
\end{split}
\\]
となるので， $\mathbf{A}$ の一番上の行を転置したベクトル $\mathbf{a}_{0}$ は， 
\\[
\mathbf{a}_{0} = \frac{1}{\sqrt{N}} \left[
\begin{matrix}
	 e^{0} \\ e^{0} \\ e^{0} \\ \vdots \\ e^{0}
\end{matrix}
 \right] \in \mathbb{R}^{N \times 1}
\\]
となること分かる．

次に $y[1]$ がどのように計算されるか考える．
$y[1]$ はどのベクトルとの演算で求まるかというと，

![](figs/a1.pdf)

の黒い部分の内積である．
定義から $y[1]$ の演算をたどると，
\\[
\begin{split}
	y[1] &= \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j \omega n 1}
	\\
	&= \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j\omega n}
	\\
	&= \frac{1}{\sqrt{N}}
		\left[
		\begin{matrix}
			e^{0} & e^{-j\omega} & e^{-j2\omega} & \cdots & e^{-j(N-1)\omega}
		\end{matrix}
		\right]
		\left[
		\begin{matrix}
			x[0] \\ x[1] \\ x[2] \\ \vdots \\ x[N-1]
		\end{matrix}
		\right]
	\\
	&= \mathbf{a}_{1}^{\top} \mathbf{x}
\end{split}
\\]
ここで，
\\[
\mathbf{a}_{1} = \frac{1}{\sqrt{N}} \left[
\begin{matrix}
	 e^{0} \\ e^{-j\omega} \\ e^{-j2\omega} \\ \vdots \\ e^{-j(N-1)\omega}
\end{matrix}
 \right] \in \mathbb{R}^{N \times 1}
\\]

同様に， $\mathbf{a}_{2}$ を求めると，
\\[
\mathbf{a}_{2} = \frac{1}{\sqrt{N}} \left[
\begin{matrix}
	 e^{0} \\ e^{-j2\omega} \\ e^{-j4\omega} \\ \vdots \\ e^{-j2(N-1)\omega}
\end{matrix}
 \right] \in \mathbb{R}^{N \times 1}.
\\]

これを，まとめると，
\\[
	\mathbf{A} = 
	\frac{1}{\sqrt{N}} 
	\left[
	\begin{matrix}
		e^{0} & e^{0} & e^{0} & \cdots & e^{0} \\
		e^{0} & e^{-j\omega} & e^{-j2\omega} & \ldots & e^{-j(N-1)\omega} \\
		e^{0} & e^{-j2\omega} & e^{-j4\omega} & \ldots & e^{-j2(N-1)\omega} \\
		& \vdots & & \ddots & \vdots \\
		e^{0} & e^{-j(N-1)\omega} & e^{-j2(N-1)\omega} & \ldots & e^{-j(N-1)(N-1)\omega} \\
	\end{matrix}
	\right]
\\]
となる．
$\mathbf{A}$ の $n$ 番目の行， $m$ 番目の列にある要素は，
\\[
	[\mathbf{A}]_{n,m} = \frac{1}{\sqrt{N}} e^{-j(m-1)(n-1)\omega}
\\]
と結構簡単に書くことができる．

## DFTを実装する

今回は，次の4つの方法を比較してもらう．

- MATLAB付属の`fft`
- `for`文を用いた自作スクリプト（`dft1`）
- `for`文+行列演算を用いた自作スクリプト（`dft2`）
- `for`文を使わない自作スクリプト（`dft3`）

まず，以下がそれぞれのスクリプトを用いた時間計測スクリプトである．

```matlab
clear all

set_N = 10.^(1:4); % 試す入力ベクトルのサイズ
n_repeat = 100;  % 繰り返し回数．繰り返して時間を計測することで誤差を小さくする

elapsed_times = zeros(length(set_N), 4);
for nn = 1:length(set_N)

    % 入力信号の作成
    x = randn(set_N(nn), 1);

    % MATLAB付属のfft
    a = zeros(n_repeat, 1);
    for rr = 1:n_repeat
        tic;
        y0 = fft(x)/sqrt(set_N(nn));
        a(rr) = toc;
    end
    elapsed_times(nn, 1) = mean(a);

    % for文による実装
    a = zeros(n_repeat, 1);
    for rr = 1:n_repeat
        tic;
        y1 = dft1(x);
        a(rr) = toc;
    end
    elapsed_times(nn, 2) = mean(a);

    % for文+行列演算による実装
    a = zeros(n_repeat, 1);
    for rr = 1:n_repeat
        tic;
        y2 = dft2(x);
        a(rr) = toc;
    end
    elapsed_times(nn, 3) = mean(a);

    % for文を使わない実装
    a = zeros(n_repeat, 1);
    for rr = 1:n_repeat
        tic;
        y3 = dft3(x);
        a(rr) = toc;
    end
    elapsed_times(nn, 4) = mean(a);
end

% それぞれの演算結果が等しいか確認
fprintf('square erros between fft and\n')
fprintf('\tdft1: %e\n', norm(y0 - y1)^2)
fprintf('\tdft2: %e\n', norm(y0 - y2)^2)
fprintf('\tdft3: %e\n', norm(y0 - y3)^2)

% 演算時間のプロット
semilogx(set_N, elapsed_times)
legend('fft', 'dft1', 'dft2', 'dft3')
xlabel('size of the input vector')
ylabel('elapsed time [s]')
```

### `dft1`

つぎに，`for`文を使ったdft1.mのスクリプトである．
アンダーバー（___）の部分を穴埋めして，完成させよう．

```matlab
function y = dft1(x)

N = length(x);
omega = -j*2*pi/N;

y = zeros(N, 1);
for kk = 0:N-1
    for nn = 0:N-1
        y(___, 1) = y(___, 1) + ___(nn+1, 1)*exp(___)/sqrt(N);
    end
end
```

### `dft2`
つぎに，`for`文+行列演算を使ったdft2.mのスクリプトである．
アンダーバー（___）の部分を穴埋めして，完成させよう．

```matlab
function y = dft2(x)

N = length(x);
omega = -j*2*pi/N;

A = zeros(N, N);
for kk = 0:N-1
    for nn = 0:N-1
        A(___, ___) = exp(___);
    end
end
A = A/sqrt(N);
y = ___;
```

### `dft3`

つぎに，`for`文を使わないdft3.mのスクリプトである．
アンダーバー（___）の部分を穴埋めして，完成させよう．

```matlab
function y = dft3(x)

N = length(x);
omega = -j*2*pi/N;
n_seq = 0:N-1;
A = exp(omega*___'*___)/sqrt(N);
y = ___;
```

## 発表してほしいこと

- スクリプトの穴埋めした完全版
- 演算時間の比較

## 演算時間の比較（参考）

![](figs/dft_time.pdf)
