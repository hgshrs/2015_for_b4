# 与えられたデータを識別する

## 目的

与えられたデータを識別する．
このとき，識別に関する一般的な手順を学ぶ．
また，認識結果ををどのように評価しなければならないのかを知る．

## 識別の手順

あるデータ $x$ が与えられたとする．
識別器は，これをクラスラベル $$y_{1}, y_{2}, \ldots, y_{C}$$ のどれかに対応付ける．
ここで，識別器を $f(\cdot)$ と表すと，
$$
\hat{y} = f(x)
$$
と書ける．
$\hat{y}$ は $x$ が属するクラスの推定ラベルである．

識別器には様々な種類があるが，何にも考えないでデータを入力しても，たいていの場合うまく行かない．
なぜなら，適当な特徴を抽出して入力しないと，識別に寄与するパターンが，その他の雑音に埋もれるからである．
このような特徴を抽出は，基本的にはこれまでの経験から行われる．
例えば，

- 画像の2値化
- 周波数変換
- 周波数フィルタ
- ダウンサンプリング
- データの選別

などがこれに当たる．

まとめると，

1. データの取得
1. 特徴抽出
1. 識別器の学習

という3つの手順が基本的に必要になる．

## 識別器の学習

基本的に識別器には学習が必要である．
ここで言う学習とは，「データ」と「そのデータが属するクラスの正解」を予め用意しておき，そのデータが上手く識別されるように，識別器内のパラメータをチューニングすることを指す．

この学習の手順を簡単に述べる．
学習のためのデータとして， $N$ 個のサンプル;
$$
\\{ x_1, y_1 \\},
\\{ x_2, y_2 \\},
\ldots,
\\{ x_N, y_N \\},
$$
が与えられたとする．
また，識別関数 $f(x)$ がパラメータ $\theta$ によって変化するパラメータとする．
基本的にはこのパラメータをいじることで，予測値;
$$
\hat{y} = f(x; \theta)
$$
ができるだけ正解に近くなるようにする．
識別器によって，どのようなパラメータを持つかは異なる．

一般的に，「 $\theta$ を出来るだけ正解に近くなるように設計する」という問題は，評価関数（コスト関数）の最適化として定式化される．
例えば，
$$
\min \quad J(\theta) = \sum_n L (y_n, \hat{y}_n) = \sum_{n} L (y_n, f(x_n, \theta)).
$$
ここで， $L$ は誤差関数と呼ばれる．
誤差関数には，二乗誤差や，それにシグモイド関数を組み合わせたものが良く用いられる．
様々な識別器が提案されているが，基本的には $f(x, \theta)$ と $L(y, \hat{y})$ の違う．
例えば，LDA (linear discriminat analysis) は誤差関数として二乗誤差を用いるが，SVM (support vector machine) はマージンと呼ばれる距離を誤差として用いている．


## 識別率を出す

「識別精度がXX [%] でした」という評価はどうやって出すべきだろうか．
重要なことは，学習（トレーニング）に用いるサンプルと検証（テスト）に使うサンプルを明確に分けておくことである．

あるデータセットを持っていて，「このデータは識別可能な特徴を持っているか調べたい」，もしくは「識別器の性能を調べたい」と考えてみる．
どちらの問題も，識別率を算出することで定量的に評価することができる．
識別率を算出する方法として，交差検定が一般的である．
データセットを学習用と検定用に分ける．
学習用のサンプルで識別器を学習し，検定用のサンプルにおける識別率を算出する．
これを何度か繰り返して，各分割で算出された識別率の平均を求める．
交差検定には，データをどのように分割するかによって，「10分割交差検定」「一つ抜き交差検定」などの名称で呼ばれる．


交差検定の分割の方法は，基本的に（一つ抜き交差検定以外は）一意性はない．
そのため，どのようにサンプルを分割するかで，得られる識別率が変化することがある．
一般的に，細かく分割すればするほど識別率の変動は小さくなる（最終的には一つ抜き法になることからも分かる）．
したがって，識別率の変動が小さい分割数を選択する必要がある．


## P300識別の例

P300は，低頻度の刺激を観測したときに現れる成分である．
このデータは，BCI Competition III [http://www.bbci.de/competition/iii/] から拝借した．
P300-spellerのデータを2クラス識別問題（P300が「あるか」「ないか」）に落としたものである．
脳波から，サンプルが観測されたときに，被験者が観測した刺激が低頻度か高頻度か予測してみよう．

### MATLAB code

```matlab
clear all

% ===================
% load data, labels, channel information
% ===================

% samples and labels
load dataset_p300
[n_samples, n_dtimes, n_channels] = size(X);
channel = 11;

% ===================
% feature extraction
% ===================

% digital filter
band = [.01, 10];
n_taps = 4;
[b, a] = butter(n_taps, band/(.5*fs));
X_ = zeros(n_samples, n_dtimes, n_channels);
for ii = 1:n_samples
    X_(ii, :, :) = filter(b, a, X(ii, :, :), [], 2);
end
figure(1);
plot((1:n_dtimes)/fs, mean(X(find(y==1), :, channel), 1),...
    (1:n_dtimes)/fs, mean(X_(find(y==1), :, channel), 1),...
    (1:n_dtimes)/fs, mean(X(find(y==0), :, channel), 1),...
    (1:n_dtimes)/fs, mean(X_(find(y==0), :, channel), 1))
legend('target', 'filtered target', 'no-taraget', 'filtered no-target')
xlabel('time [sec]')
ylabel('ampltude [\mu V]')
X = X_;

% down-sample
downsampled_fs = 24;
X_ = zeros(n_samples, downsampled_fs*n_dtimes/fs, n_channels);
down_factor = fs/downsampled_fs;
for ii = 1:n_samples
    X_(ii, :, :) = X(ii, 1:down_factor:n_dtimes, :);
end
X = X_;
fs = downsampled_fs;
n_dtimes = size(X, 2);

% time window
window = [.1 .6]; % [sec]
window_sample = window*fs;
time_sequence = (1:n_dtimes)/fs;
idxs = min(find(time_sequence > window(1))):max(find(time_sequence < window(2)));
X = X(:, idxs, :);

% select the channels
selected_channels = [4, 11, 18, 27, 34, 51, 58, 62, 64]; % Fcz, Cz, Cpz, Fpz, Afz, Fz, Pz, Poz, Oz, Iz
X = X(:, :, selected_channels);

% vectorize
[n_samples, n_dtimes, n_channels] = size(X);
X = reshape(X, n_samples, n_dtimes*n_channels);


% ===================
% classification and validation
% ===================

% divide traning and test data
n_trains = floor(n_samples*.5); % the number of the traning samples
X_train = X(1:n_trains, :, :); % assign the formar n_trains samples as traning samples
y_train = y(1:n_trains); % labels for traning samples
X_test = X(n_trains+1:n_samples, :, :); % assign the remaining samples as testing samples
y_test = y(n_trains+1:n_samples); % labels for testing samples

% learn a classifier (LDA)
classifier = fitcdiscr(X_train, y_train);

% predice traing samples (calibration)
predicted_y = predict(classifier, X_train); % predicted labels for the traning samples
correct_flags = not(xor(y_train, predicted_y)); % extract the samples correctly classified
acc = sum(correct_flags)/length(y_train)*100; % [%]
fprintf('Accuracy (calibration): %f [%%]\n', acc) % display the calibration accuracy

% predice testing samples (validation)
predicted_y = predict(classifier, X_test); % predicted labels for the testing samples
correct_flags = not(xor(y_test, predicted_y)); % extract the samples correctly classified
acc = sum(correct_flags)/length(y_test)*100; % [%]
fprintf('Accuracy (validation): %.3f [%%]\n', acc) % display the calibration accuracy

```

### `# loading` `# samples and labels`

- `X`: (サンプル数)x(時間サンプル数)x(チャンネル数) の行列．観測脳波．
- `y`: サイズ (サンプル数) のベクトル．正解のラベル．
- `fs`: サンプリング周波数


### `# feature extraction` `# digital filter`

観測信号を周波数帯域 `band` でフィルタリング．
ここでは，IIRフィルタの一種であるバターワースフィルタ（`butter`）を使用した．

### `# feature extraction` `# down-sampling`

時間サンプル数が大きいため，ダウンサンプルする．
間引きによってダウンサンプリングしている．
どの程度間引きされるかは`down_factor`によって決まる．

### `# feature extraction` `# windowing`

事象関連電位（ERP）が発生しているところだけ切り取る．

### `# feature extraction` `# select the channels`

電極を選択して，電極数を減らす．

### `# feature extraction` `# vectorize`

基本的に識別器にはベクトルを入力しないといけないので，行列をベクトル化している．

- `X`: (サンプル数)x(時間サンプル数xチャンネル数) の行列．観測脳波．

### `# classification and validation` `# divide training and test data`

データを学習用と検証用に分割する．
ここでは，最初の`n_trains`分のサンプルを学習用としているが，特に理由がない場合は，ランダムに選ぶのが望ましい．
ここでは交差検定は行っていない．

### `# classification and validation` `# learning with LDA`

識別器を学習する．
ここでは，線形判別器（Linear discriminant analysis; LDA）`fitcdiscr`を使用した．

### `# classification and validation` `# prediction`

まず，学習用データを識別して識別率（`Accuracy (calibration)`）を算出する．
次に，検証用データを識別して識別率（`Accuracy (validation)`）を算出する．

% 学習用データの識別率と検証用データの識別率を比べることで，過学習の発生などを推測することができる

## やってもらうこと

1. 交差検定を実装せよ．10分割でも一つ抜きでもどれでも良い
1. Matlab Statistics Toolbox の機械学習のページ (http://jp.mathworks.com/help/stats/supervised-learning.html) を参考に，LDA以外の識別器による識別率を交差検定によって求めよ
