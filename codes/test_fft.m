clear all

set_N = 10.^(1:4); % 試す入力ベクトルのサイズ
n_repeat = 100;  % 繰り返し階数．繰り返して時間を計測することで誤差を小さくする

elapsed_times = zeros(length(set_N), 4);
for nn = 1:length(set_N)

    % 入力信号の作成
    x = randn(set_N(nn), 1);

    % MATLAB付属のfft
    a = zeros(n_repeat, 1);
    for rr = 1:n_repeat
        tic;
        y0 = fft(x)/sqrt(set_N(nn));
        a(rr) = toc;
    end
    elapsed_times(nn, 1) = mean(a);

    % for文による実装
    a = zeros(n_repeat, 1);
    for rr = 1:n_repeat
        tic;
        y1 = dft1(x);
        a(rr) = toc;
    end
    elapsed_times(nn, 2) = mean(a);

    % for文+行列演算による実装
    a = zeros(n_repeat, 1);
    for rr = 1:n_repeat
        tic;
        y2 = dft2(x);
        a(rr) = toc;
    end
    elapsed_times(nn, 3) = mean(a);

    % for文を使わない実装
    a = zeros(n_repeat, 1);
    for rr = 1:n_repeat
        tic;
        y3 = dft3(x);
        a(rr) = toc;
    end
    elapsed_times(nn, 4) = mean(a);
end

% それぞれの演算結果が等しいか確認
fprintf('square erros between fft and\n')
fprintf('\tdft1: %e\n', norm(y0 - y1)^2)
fprintf('\tdft2: %e\n', norm(y0 - y2)^2)
fprintf('\tdft3: %e\n', norm(y0 - y3)^2)

% 演算時間のプロット
semilogx(set_N, elapsed_times)
legend('fft', 'dft1', 'dft2', 'dft3')
xlabel('size of the input vector')
ylabel('elapsed time [s]')
