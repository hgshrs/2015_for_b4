clear all

% ===================
% load data, labels, channel information
% ===================

% samples and labels
load dataset_p300
[n_samples, n_dtimes, n_channels] = size(X);
channel = 11;

% ===================
% feature extraction
% ===================

% digital filter
band = [.01, 10];
n_taps = 4;
[b, a] = butter(n_taps, band/(.5*fs));
X_ = zeros(n_samples, n_dtimes, n_channels);
for ii = 1:n_samples
    X_(ii, :, :) = filter(b, a, X(ii, :, :), [], 2);
end
figure(1);
plot((1:n_dtimes)/fs, mean(X(find(y==1), :, channel), 1),...
    (1:n_dtimes)/fs, mean(X_(find(y==1), :, channel), 1),...
    (1:n_dtimes)/fs, mean(X(find(y==0), :, channel), 1),...
    (1:n_dtimes)/fs, mean(X_(find(y==0), :, channel), 1))
legend('target', 'filtered target', 'no-taraget', 'filtered no-target')
xlabel('time [sec]')
ylabel('ampltude [\mu V]')
X = X_;

% down-sample
downsampled_fs = 24;
X_ = zeros(n_samples, downsampled_fs*n_dtimes/fs, n_channels);
down_factor = fs/downsampled_fs;
for ii = 1:n_samples
    X_(ii, :, :) = X(ii, 1:down_factor:n_dtimes, :);
end
X = X_;
fs = downsampled_fs;
n_dtimes = size(X, 2);

% time window
window = [.1 .6]; % [sec]
window_sample = window*fs;
time_sequence = (1:n_dtimes)/fs;
idxs = min(find(time_sequence > window(1))):max(find(time_sequence < window(2)));
X = X(:, idxs, :);

% select the channels
selected_channels = [4, 11, 18, 27, 34, 51, 58, 62, 64]; % Fcz, Cz, Cpz, Fpz, Afz, Fz, Pz, Poz, Oz, Iz
X = X(:, :, selected_channels);

% vectorize
[n_samples, n_dtimes, n_channels] = size(X);
X = reshape(X, n_samples, n_dtimes*n_channels);


% ===================
% classification and validation
% ===================

% divide traning and test data
n_trains = floor(n_samples*.5); % the number of the traning samples
X_train = X(1:n_trains, :, :); % assign the formar n_trains samples as traning samples
y_train = y(1:n_trains); % labels for traning samples
X_test = X(n_trains+1:n_samples, :, :); % assign the remaining samples as testing samples
y_test = y(n_trains+1:n_samples); % labels for testing samples

% learn a classifier (LDA)
classifier = fitcdiscr(X_train, y_train);

% predice traing samples (calibration)
predicted_y = predict(classifier, X_train); % predicted labels for the traning samples
correct_flags = not(xor(y_train, predicted_y)); % extract the samples correctly classified
acc = sum(correct_flags)/length(y_train)*100; % [%]
fprintf('Accuracy (calibration): %f [%%]\n', acc) % display the calibration accuracy

% predice testing samples (validation)
predicted_y = predict(classifier, X_test); % predicted labels for the testing samples
correct_flags = not(xor(y_test, predicted_y)); % extract the samples correctly classified
acc = sum(correct_flags)/length(y_test)*100; % [%]
fprintf('Accuracy (validation): %.3f [%%]\n', acc) % display the calibration accuracy

