slidenumbers: true

## 数式をMATLABで実装する

### 東 広志

### 2015年6月

---

# 目的

- 推奨される数式の実装
	- MATLAB
	- Toolbox
	- 公開されているコード
- 怖がらずに新しい手法（数式）を実装する方法を身につける
- MATLABが得意な行列演算を意識する

---

# 数式を読み解くヒント
## ルールを知る

- 小文字+斜体 $$x, y, z$$: 変数
- 大文字+斜体 $$X, Y, Z$$: 定数，確率変数
- 小文字+太字 $$\mathbf{x}, \mathbf{y}, \mathbf{z}$$: ベクトル
- 大文字+太字 $$\mathbf{X}, \mathbf{Y}, \mathbf{Z}$$: 行列
- 大文字+カリグラフィ $$\mathcal{X}, \mathcal{Y}, \mathcal{Z}$$: 集合

---

# 数式を読み解くヒント
## どのルールを採用するか？

基本的には自由だけど

- その分野の一般的なルールを採用する
- 文章中でルールを変更しない

---

# ターゲットとなる数式
## 離散フーリエ変換（DFT）

- Descrete Fourier Transform (DFT)
- 周波数解析で最も良く使われる線形写像

---

# DFTの定義

$$
	y[k] = \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j\frac{ 2 \pi k n}{N} },
	<!-- \quad k = 0, \ldots, N-1 -->
$$

- $$x[n]$$: 時間インデックス $$n$$ で観測された信号，
- $$y[k]$$: 周波数インデックス $$k$$ における離散フーリエ変換後の信号，
- $$N$$: 信号長， $$N \in \mathbb{Z}^{+}$$

---

# DFTの定義（続き）


- $$\frac{2 \pi}{N}$$は定数なので，$$\omega = \frac{2 \pi }{N}$$

$$
	y[k] = \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j \omega kn },
	\quad k = 0, \ldots, N-1
$$

---

# MATLABにおけるDFTの実装

- 高速フーリエ変換（Fast Fourier Transform: FFT）が，`fft`として実装済み
- 使い方

```matlab
>> x = randn(100, 1)
>> y = fft(x)
```

- FFTはとにかく速い

---

# 行列演算としてDFTを実装する

1. 行列のサイズを確認する
2. 行列の要素を求める

---

# 行列のサイズを確認する
## 入力信号: $$\textbf{x}$$

- 定義式の$$\sum$$の範囲に注目すると，
- $$x[0], x[1], x[2], \ldots, x[N-2], x[N-1]$$
- $$N$$個の値を使用
- つまり，$$N$$次元ベクトルに値を格納可能

---

# 行列のサイズを確認する
## 入力信号: $$\textbf{x}$$

![inline](figs/1.pdf)

---

# 行列のサイズを確認する
## 出力信号: $$\textbf{y}$$

- $$k$$の範囲が$$0, 1, \ldots, N-1$$なので，
- $$y[0], y[1], y[2], \ldots, y[N-2], y[N-1]$$
- $$N$$個の値が出力される
- つまり，$$N$$次元ベクトルに値を格納可能

---

# 行列のサイズを確認する
## 加算$$\sum$$を含む演算

基本的に

$$
	\mathbf{y} = \mathbf{A} \mathbf{x}
$$

![inline](figs/2.pdf)

---

# 行列のサイズを確認する
## 数式$$\mathbf{y} = \mathbf{A} \mathbf{x}$$を満たす$$\textbf{A}$$のサイズ

$$1 \times 1$$か$$N \times N$$

![inline](figs/1x1.pdf)![inline](figs/nxn.pdf)

---

# 行列のサイズを確認する

さすがに$$1 \times 1$$は無いので

![inline](figs/nxn.pdf)

を採用

---

# 行列$$\textbf{A}$$の中身

- 定義式を読み解いて行列$$\textbf{A}$$の要素を求める
- 理想的には，すべての要素が一つの数式（行・列番号を変数とする）で表せれば良い

$$
[\textbf{A}]_{n,m} = f(n, m)
$$

---

# 出力$$y[0]$$を考える

![inline](figs/a0.pdf)

---

# 定義から$$y[0]$$の演算をたどる

$$
\begin{split}
	y[0] &= \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j\omega n 0 }
	\\
	&= \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{0}
	\\
	&= \frac{1}{\sqrt{N}}
		\left[
		\begin{matrix}
			e^{0} & e^{0} & e^{0} & \cdots & e^{0}
		\end{matrix}
		\right]
		\left[
		\begin{matrix}
			x[0] \\ x[1] \\ x[2] \\ \vdots \\ x[N-1]
		\end{matrix}
		\right]
	\\
	&= \mathbf{a}_{0}^{\top} \mathbf{x}
\end{split}
$$

---

# ベクトル$$\textbf{a}_{0}$$


$$
\mathbf{a}_{0} = \frac{1}{\sqrt{N}} \left[
\begin{matrix}
	 e^{0} \\ e^{0} \\ e^{0} \\ \vdots \\ e^{0}
\end{matrix}
 \right] \in \mathbb{R}^{N \times 1}
$$

---

# 出力$$y[1]$$を考える

![inline](figs/a1.pdf)

---

# 定義から$$y[1]$$の演算をたどる

$$
\begin{split}
	y[1] &= \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j \omega n 1}
	\\
	&= \frac{1}{\sqrt{N}} \sum_{n=0}^{N-1} x[n] e^{-j\omega n}
	\\
	&= \frac{1}{\sqrt{N}}
		\left[
		\begin{matrix}
			e^{0} & e^{-j\omega} & e^{-j2\omega} & \cdots & e^{-j(N-1)\omega}
		\end{matrix}
		\right]
		\left[
		\begin{matrix}
			x[0] \\ x[1] \\ x[2] \\ \vdots \\ x[N-1]
		\end{matrix}
		\right]
	\\
	&= \mathbf{a}_{1}^{\top} \mathbf{x}
\end{split}
$$

---

# ベクトル$$\textbf{a}_{1}$$


$$
\mathbf{a}_{1} = \frac{1}{\sqrt{N}} \left[
\begin{matrix}
	 e^{0} \\ e^{-j\omega} \\ e^{-j2\omega} \\ \vdots \\ e^{-j(N-1)\omega}
\end{matrix}
 \right] \in \mathbb{R}^{N \times 1}
$$

---

# ベクトル$$\textbf{a}_{2}$$


$$
\mathbf{a}_{2} = \frac{1}{\sqrt{N}} \left[
\begin{matrix}
	 e^{0} \\ e^{-j2\omega} \\ e^{-j4\omega} \\ \vdots \\ e^{-j2(N-1)\omega}
\end{matrix}
 \right] \in \mathbb{R}^{N \times 1}
$$

---

# 行列$$\textbf{A}$$


$$
	\mathbf{A} = 
	\frac{1}{\sqrt{N}} 
	\left[
	\begin{matrix}
		e^{0} & e^{0} & e^{0} & \cdots & e^{0} \\
		e^{0} & e^{-j\omega} & e^{-j2\omega} & \ldots & e^{-j(N-1)\omega} \\
		e^{0} & e^{-j2\omega} & e^{-j4\omega} & \ldots & e^{-j2(N-1)\omega} \\
		& \vdots & & \ddots & \vdots \\
		e^{0} & e^{-j(N-1)\omega} & e^{-j2(N-1)\omega} & \ldots & e^{-j(N-1)(N-1)\omega} \\
	\end{matrix}
	\right]
$$

- $$\mathbf{A}$$の$$n$$番目の行，$$m$$番目の列にある要素は，

$$
	[\mathbf{A}]_{n,m} = \frac{1}{\sqrt{N}} e^{-j(m-1)(n-1)\omega}
$$

---

# DFTを実装する

次の4つの実装における演算結果と演算時間を比較してもらう

- MATLAB付属の`fft`
- `for`文を用いた自作スクリプト（`dft1`）
- `for`文と行列演算を用いた自作スクリプト（`dft2`）
- `for`文を使わない自作スクリプト（`dft3`）

---

# `dft1`

```matlab
function y = dft1(x)

N = length(x);
omega = -j*2*pi/N;

y = zeros(N, 1);
for kk = 0:N-1
    for nn = 0:N-1
        y(___, 1) = y(___, 1)...
			+ ___(nn+1, 1)*exp(___)/sqrt(N);
    end
end
```

---

# `dft2`

```matlab
function y = dft2(x)

N = length(x);
omega = -j*2*pi/N;

A = zeros(N, N);
for kk = 0:N-1
    for nn = 0:N-1
        A(___, ___) = exp(___);
    end
end
A = A/sqrt(N);
y = ___;
```

---

# `dft3`

```matlab
function y = dft3(x)

N = length(x);
omega = -j*2*pi/N;
n_seq = 0:N-1;
A = exp(omega*___'*___)/sqrt(N);
y = ___;
```

---

# 発表してほしいこと

- スクリプトを穴埋めした完全版
- 演算時間の比較

---

# 演算時間の比較（参考）

![inline 120%](figs/dft_time.pdf)
